<?php

# Extension Manager/Repository config file for ext: "t3themes_phantom"

$EM_CONF[$_EXTKEY] = [
    'title' => 'TYPO3 Themes - Phantom',
    'description' => 'HTML5UP Free Template Theme Integration for EXT:t3cms. Brings full styled and configured website. Custom content elements maybe available in this theme.',
    'category' => 'distribution',
    'author' => 'Salvatore Eckel',
    'author_email' => 'salvaracer@gmx.de',
    'state' => 'stable',
    'internal' => '',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '2.3.0',
    'constraints' => [
        'depends' => [
            'typo3' => '7.6.18-9.99.99',
            't3cms' => '2.0.1'
        ],
        'conflicts' => [],
        'suggests' => [
            't3content_phantom' => '2.3.0'
        ],
    ],
];
