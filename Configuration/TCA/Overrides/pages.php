<?php
defined('TYPO3_MODE') || die();

$extensionKey = 't3themes_phantom';

/***************
 * Register PageTS
 */

// BackendLayouts
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TSconfig/Mod/WebLayout/BackendLayouts.txt',
    'T3themes Phantom - Backend Layouts'
);

// Customize RTE
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::registerPageTSConfigFile(
    $extensionKey,
    'Configuration/TSconfig/RTE.txt',
    'T3themes Phantom - CKEditor Configuration'
);
