<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function($extKey)
    {

    	# Backend Module 1
        if (TYPO3_MODE === 'BE' && !(TYPO3_REQUESTTYPE & TYPO3_REQUESTTYPE_INSTALL)) {
		    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		        'SalvatoreEckel.' . $extKey,
		        'help',
		        't3themesphantom',		// Name of the module
		        'after:aboutmodules',
		        array(
		            'Phantom' => 'contentElements,liveDemo',
		        ),
		        array(
					'access'    => 'user,group',
					'icon'      => 'EXT:' . $extKey . '/ext_icon.svg',
		            'labels' => 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang.xlf',
		        )
		    );
		}

    },
    $_EXTKEY
);
