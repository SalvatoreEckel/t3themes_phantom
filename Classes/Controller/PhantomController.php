<?php
namespace SalvatoreEckel\T3themesPhantom\Controller;

/*
use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Messaging\AbstractMessage;
use \TYPO3\CMS\Extbase\Utility\DebuggerUtility;
*/

/**
 * phantomController
 */
class PhantomController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * action contentElements
     *
     * @return void
     */
    public function contentElementsAction()
    {
    }

    /**
     * action liveDemo
     *
     * @return void
     */
    public function liveDemoAction()
    {
        $this->view->assign('extkey', 't3themes_phantom');
    }

}
